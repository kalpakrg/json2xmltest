var json2xml = require('json2xml');

var bad = {
	parent: {
		$attributes: {
			pa1: 'pav1',
			pa2: 'pav2' 
		},

		child1: {
			$attributes: {
				c1a1: "c1a1v",
				c1a2: "c1a2v",
			}
		},

		child2: {
			$attributes: {
				c2a1: "c2a1v",
				c2a2: "c2a2v",
			}
		}
	}
};

var good = {
	parent: [
		{
			child1: '',
			$attributes: {
				c1a1: "c1a1v",
				c1a2: "c1a2v",
			}
		},

		{
			child2: '',
			$attributes: {
				c2a1: "c2a1v",
				c2a2: "c2a2v",
			}
		}
	],
	$attributes: {
		pa1: 'pav1',
		pa2: 'pav2' 
	}
	
};

var output = json2xml(bad, { attributes_key: '$attributes' });

console.log(JSON.stringify(output));

var output = json2xml(good, { attributes_key: '$attributes' });

console.log(JSON.stringify(output));